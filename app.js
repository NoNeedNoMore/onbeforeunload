'use strict';

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const port = process.env.PORT || 1337;

app
    .use ( express.static(__dirname + '/public'))
    .get ( '/', (req, res) => res.redirect('index.html'))
    .all ( '*', (req, res, next) => next(new Error('Service not exists')))
    .use ( (err, req, res, next) => res.status(200).json({err: err.message}));

server.listen(port, () => {console.log(`${new Date()} Listening at ${port}`);});
